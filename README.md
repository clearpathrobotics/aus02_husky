This package is intended to support the University at Austin Texas Husky. It is equipped with dual UR5 manipulators and a SICK LMS511 LIDAR.

The ur_dual_arm.launch file may be used to launch two instances of the UR5 driver, linked to static IPs.  The robot will also publish the correct
transforms for the arms and LIDAR for viewing in rviz.

Since the on-board computer is likely to be ready far before the arms themselves, this launch file should be activated manually once the arms have
been initialized.

When viewing the robot model remotely in rviz, the default husky_viz view_robot.launch will launch an instance of joint_state_publisher, which
will conflict with the "real" joints being published by the arms.  Therefore, the replacement view_robot.launch file, located in the launch directory,
may be used.

The LIDAR position declared in the included xacro is meant for the lower mounting position.  If the upper position is used, change the z position to 
0.182.

